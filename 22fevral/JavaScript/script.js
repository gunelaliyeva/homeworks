"use strict";

/*   W A R M U P
* Write a function for showing the random words from input string.
* UI - input[type=text], okay button, empty span
*
* All UI elements should be created and placed on the page using JS.
*
* The task is:
* User can enter a string with any words in it. All the words should be separated with ' '.
* Okay button is disabled until at least 1 character entered in input.
* After click by okay button - random word from input string should be sowed up in a span.
* Random words that was showed needs to be saved in some pull, because your task is to show different random words.
* If all words from input string was successfully showed - okay button becomes disabled, text inside span becomes red.
* */

function RandomString(containerSelector) {
    this.ui = {
        container: document.querySelector(containerSelector)
    };
    this.renderUI();
    this.assignEvents();
}
RandomString.prototype.renderUI=function () {...};
RandomString.prototype.assignEvents =function () {...};

RandomString.prototype.renderUI = function () {
    let {ui} = this;
    const enterText = document.createElement('input'),
        okBtn = document.createElement('button'),
        msgText = document.createElement('span');

    enterText.classList.add('random-str-input');
    okBtn.classList.add('random-str-btn');
    msgText.classList.add('random-str-msg');

    ui.container.append(enterText, okBtn, msgText);
    ui = {
        ...ui,
        enterText,
        okBtn,
        msgText
    };
};

const r = new RandomString('.random-container');
r.renderUI();