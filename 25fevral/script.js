// There is some `mailStorage` - a little simulation of real storage with email letters.

let mailStorage = null; // null is initial value for out mailStorage

if(localStorage.getItem('emails')) { //check if there is any values in localStorage under the key 'emails;
    mailStorage = JSON.parse( //it will parse value from the localStorage from string into object
        localStorage.getItem('emails')
    );
} else { //it will work only if there is no values under the 'emails' key in localStorage
    mailStorage = [
        {
            subject: "Hello world",
            from: "gogi@gogimail.go",
            to: "lolabola@ui.ux",
            text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
        },
        {
            subject: "How could you?!",
            from: "gogi@gogimail.go",
            to: "ingeneer@nomail.here",
            text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
        },
        {
            subject: "Acces denied",
            from: "gogi@gogimail.go",
            to: "gogidoe@somemail.nothing",
            text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
        }
    ]; //setting up the initial value of mailStorage
    localStorage.setItem('emails', JSON.stringify(mailStorage)) //saving it into localStorage under the key 'emails'
}


/*** THE TASK IS ***
 * 0) Rewrite mailStorage in the way to keep all the email in localStorage under the 'emails' key. If there is no any value for 'emails' key in localStorage - feel free to assign it manually.
 *
 * 1) Show up all the letters on the screen. Each and every of them should be created via JS.
 *    Every letter has hidden text. Text needs to be shown, only after the user clicks on the letters item.
 *    It is necessary to use only one event listener for container with letters.
 *
 * 2) Implement toggle text effect. It means that only one text on only one email can appears at the same time.
 *    If user made a click on the email, which doesn't have any showed text - you need to close current opened text, and only then open the text for the email that was just clicked.
 *
 * 3) Create "New Mail" button. After pressing this button user need to see a modal window with the form for create mew email letter.
 *    Fields for this form are:
 *        Email Title,
 *        To (email of the recipient),
 *        letter text (up to 500 symbols),
 *        "Send" button.
 * By the way, you need automatically fill the 'from' property for each letter, and it will be "gogi@gogimail.go".
 *    If user want to close the modal window, he can do this, by clicking on the cross sign at the top right corner of the modal window.
 *    Modal window size is 500px both width and height. It shows up at the bottom right corner of the page, user can get access to any other part of functionality while modal window is showing.
 *
 * 4) Every letter needs to have a "Delete" button, which will delete this particular letter both from the page and from the mailStorage.
 *
 * 5) Add some timeout to delete button, so it'll delete the email from storage only after 1 second.
 *
 * 6) Add simulation of loading emails. Show "Loading..." or any preloader animation during first 2 seconds after reloading the page.
 *
 * 7) Rewrite all your code into constructor function, where should be:
 *  - private property for storing copy of all emails
 *  - at least 2 private methods for temporary operations inside the object
 * */

const emails = document.getElementById('emails');
let openedTextIndex=null;

mailStorage.forEach(email => {
    /*creation of the elements*/
    const emailContainer = document.createElement('div'),
        subject = document.createElement('h3'),
        from = document.createElement('p'),
        to = document.createElement('p'),
        text = document.createElement('p');

    /*putting content from object to DOM elements*/
    subject.innerText = email.subject;
    from.innerText = email.from;
    to.innerText = email.to;
    text.innerText = email.text;

    /*hiding the text element from the users eye*/
    text.hidden = true;

    /*put all nested elements into container*/
    emailContainer.append(
        subject,
        from,
        to,
        text
    );

    /*add fancy styles for out email elements*/
    emailContainer.classList.add('email-item');
    subject.classList.add('email-item-headline');
    from.classList.add('email-item-info');
    from.classList.add('email-item-from');
    to.classList.add('email-item-info');
    to.classList.add('email-item-to');
    text.classList.add('email-item-message');

    /*place created email element into document*/
    emails.append(emailContainer);
});

emails.addEventListener('click', event => {
    const pressedItem = event.target;

    if(pressedItem.classList.contains('email-item')) {
        const emailsWrapper=pressedItem.parentElement;
        const allEmailItems=emailsWrapper.children;
        if(allEmailItems[openedTextIndex])
        {
            allEmailItems[openedTextIndex].lastChild.hidden=true;
        }
        pressedItem.lastChild.hidden=!pressedItem.lastChild.hidden;
        openedTextIndex=[...allEmailItems].indexOf(pressedItem);
    }   else if(pressedItem.parentElement.classList.contains('email-item')) {
       // pressedItem.parentElement.lastChild.hidden = !pressedItem.parentElement.lastChild.hidden
        const emailsWrapper=pressedItem.parentElement.parentElement;
        const allEmailItems=emailsWrapper.children;
        if(allEmailItems[openedTextIndex])
        {
            allEmailItems[openedTextIndex].lastChild.hidden=true;
        }
        pressedItem.parentElement.lastChild.hidden=!pressedItem.parentElement.lastChild.hidden;
        openedTextIndex=[...allEmailItems].indexOf(pressedItem.parentElement);
    }
});
